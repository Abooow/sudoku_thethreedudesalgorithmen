import os


# ---------------------------- Constants
BLUE = '\u001b[34m' # value for a blue color
RESET = '\u001b[0m' # value for resetting the color


# The original unsolved sudoku
sudoku = [
    [8, 7, 6, 9, 0, 0, 0, 0, 0],
    [0, 1, 0, 0, 0, 6, 0, 0, 0],
    [0, 4, 0, 3, 0, 5, 8, 0, 0],
    [4, 0, 0, 0, 0, 0, 2, 1, 0],
    [0, 9, 0, 5, 0, 0, 0, 0, 0],
    [0, 5, 0, 0, 4, 0, 3, 0, 6],
    [0, 2, 9, 0, 0, 0, 0, 0, 8],
    [0, 0, 4, 6, 9, 0, 1, 7, 3],
    [0, 0, 0, 0, 0, 1, 0, 0, 4]]


# -------------------------------------------------------------------------------------------- <Helper Funtions>
def count_empty(matrix) -> int:
    ''' Counts all 0's in the matrix

    returns: the amount of zeros (int)

    param matrix: a 2d array (int[][])
    '''

    return sum([i.count(0) for i in matrix])


def get_numbers_in_row(matrix, y) -> list:
    ''' Get all numbers in the specified row of the matrix

    returns: all numbers in row (y) except 0's of the matrix (list of int)

    param matrix: a 2d array (int[][])
    param y: row (int)
    '''

    return [i for i in matrix[y] if i != 0]


def get_numbers_in_column(matrix, x) -> list:
    ''' Get all numbers in the specified column of the matrix

    returns: all numbers in column (x) except 0's of the matrix (list of int)

    param matrix: a 2d array (int[][])
    param x: column (int)
    '''

    return [i[x] for i in matrix if i[x] != 0]


def get_numbers_in_square(matrix, x, y) -> list:
    ''' Get all numbers in the same square as cell (x, y) of the matrix

    returns: all numbers in the same square as cell (x, y) except 0's of the matrix (list of int)

    param matrix: a 2d array (int[][])
    param x: x coordinate in the matrix (int)
    param y: y coordinate in the matrix (int)
    '''

    total = []
    for y_ in range(int(y / 3) * 3, int(y / 3) * 3 + 3):
        for x_ in range(int(x / 3) * 3, int(x / 3) * 3 + 3):
            if matrix[y_][x_] != 0:
                total.append(matrix[y_][x_])
    return total


def get_possible_numbers(matrix, x, y):
    ''' Finds all possible numbers in the given cell

    return: all possible numbers in the given cell (list of int)

    param matrix: a 2d array (int[][])
    param x: x coordinate in the matrix (int)
    param y: y coordinate in the matrix (int)
    '''

    total_possible = get_numbers_in_row(matrix, y) + get_numbers_in_column(matrix, x) + get_numbers_in_square(matrix, x, y)
    return list({1, 2, 3, 4, 5, 6, 7, 8, 9} - set(total_possible))
# -------------------------------------------------------------------------------------------- </Helper Funtions>


def solve(sudoku) -> list:
    ''' Solves an easy sudoku

    returns: a copy of the solved sudoku (int[][])

    param sudoku: the sudoku to solve (int[][])
    '''

    solved_sudoku = [[j for j in i] for i in sudoku] # a copy of the sudoku
    empty_cells = count_empty(solved_sudoku)
    while empty_cells != 0:
        restart = False
        for y in range(len(solved_sudoku)):
            for x in range(len(solved_sudoku[y])):
                if solved_sudoku[y][x] != 0:
                    continue
                possible = get_possible_numbers(solved_sudoku, x, y)
                if len(possible) == 1:
                    solved_sudoku[y][x] = possible[0]
                    empty_cells -= 1
                    restart = True
                    break
            if restart:
                break
    return solved_sudoku


def print_sudoku(sudoku, original=None):
    ''' Prints out the sudoku to the console.

    returns: None

    param sudoku: the sudoku to print (int[][])
    param original: the unsolved sudoku (int[][])
    '''
    for y in range(len(sudoku)):
        if y % 3 == 0 and y != 0:
            print("-" * 21) 
        for x in range(len(sudoku[y])):
            if x % 3 == 0 and x != 0:
                print("| ", end="") 

            if sudoku[y][x] == 0:
                 print("  ", end = "")
            else:
                if original == None or sudoku[y][x] == original[y][x]:
                    print(sudoku[y][x], end=" ")
                else:
                    print(f'{BLUE}{sudoku[y][x]}{RESET}', end=" ")
        print()


def main():
    ''' The main funtion
    '''

    # Solve the sudoku
    solved_sudoku = solve(sudoku)

    # Print out the unsolved sudoku and then print the solved one
    print('Unsolved sudoku')
    print('-' * 21)
    print_sudoku(sudoku)
    print()
    print('Solved sudoku')
    print('-' * 21)
    print_sudoku(solved_sudoku, sudoku)

    print()


# Call the main funcion
if __name__ == '__main__':
    os.system('color') 
    main()
